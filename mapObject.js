function mapObject(obj,cb){
    let newArray ={};
    for (let key in obj){
        newArray[key]=cb(obj[key]);
    }
    return newArray;
}

export default mapObject;