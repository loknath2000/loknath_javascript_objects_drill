function invert(obj){
    let newArray={};
    for (let key in obj){
        newArray[obj[key]]=key;
    }
    return newArray;
}

export default invert;